import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChampionDashboardComponent } from './components/champion-dashboard/champion-dashboard.component';
import { ChampionDetailComponent } from './components/champion-detail/champion-detail.component';
import { ChampionRoutingModule } from './champion-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    ChampionDashboardComponent,
    ChampionDetailComponent
  ],
  imports: [
    CommonModule,
    ChampionRoutingModule,
    SharedModule,
    RouterModule
  ]
})
export class ChampionModule { }
