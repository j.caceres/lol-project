import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChampionDashboardComponent } from './components/champion-dashboard/champion-dashboard.component';
import { ChampionDetailComponent } from './components/champion-detail/champion-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ChampionDashboardComponent,
  },
  {
    path: ':championName',
    component: ChampionDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChampionRoutingModule { }
