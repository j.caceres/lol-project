import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Champion } from 'src/app/core/classes/Champion';
import { StorageDataService } from '../../../core/services/storage-data.service';
@Component({
  selector: 'app-champion-dashboard',
  templateUrl: './champion-dashboard.component.html',
  styleUrls: ['./champion-dashboard.component.scss']
})
export class ChampionDashboardComponent implements OnInit {


  champions: Champion[];
  championsFiltered: Champion[] = [];
  championExists: boolean;

  searchPattern: string;

  componentSubscriptions: { [key: string]: Subscription } = {};

  showSpinner = false;

  constructor(
    private storageDataService: StorageDataService,
  ) { }

  ngOnInit(): void {

    this.showSpinner = true;
    this.storageDataService.getAllChampions();
    this.suscribestorageDataService();
  }

  suscribestorageDataService(): void {
    this.componentSubscriptions.champion$ = this.storageDataService.champions$.subscribe(
      champs => {

        if (champs.length) {
          this.champions = champs;
          this.championsFiltered = champs
          setTimeout(() => this.showSpinner = false, 1500);
        }
      }
    );
  }

  search(pattern: string) {
    if (pattern) {
      this.championsFiltered = this.champions.filter(champion => champion.name.toLocaleLowerCase().includes(pattern));
    } else {
      this.championsFiltered = this.champions;
    }
  }
}
