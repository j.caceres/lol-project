import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Champion } from 'src/app/core/classes/Champion';
import { ChampionService } from 'src/app/core/services/champion.service';

@Component({
  selector: 'app-champion-detail',
  templateUrl: './champion-detail.component.html',
  styleUrls: ['./champion-detail.component.scss']
})
export class ChampionDetailComponent implements OnInit {

  champion: Champion;
  championExists: boolean;
  serializedPlayRates: [
    {
      position: 'Top',
      value: number
    },
    {
      position: 'Jungle',
      value: number
    },
    {
      position: 'Middle',
      value: number
    },
    {
      position: 'Bottom',
      value: number
    },
    {
      position: 'Support',
      value: number
    },
  ] = [
      {
        position: 'Top',
        value: 0
      },
      {
        position: 'Jungle',
        value: 0
      },
      {
        position: 'Middle',
        value: 0
      },
      {
        position: 'Bottom',
        value: 0
      },
      {
        position: 'Support',
        value: 0
      },
    ]

  globalPickRate = 0;
  showSpinner = false;
  componentSubscriptions: { [key: string]: Subscription } = {};

  constructor(

    private championService: ChampionService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.subscribeToChampionParam();
  }

  subscribeToChampionParam(): void {
    this.componentSubscriptions.paramMap$ = this.activatedRoute.paramMap.subscribe(
      params => {
        this.showSpinner = true;
        this.championExists = true;
        this.championService.getChampionByName(params.get('championName')).subscribe(
          champion => {
            this.serializePlayRates(champion.play_rates)
            this.champion = champion;
            this.showSpinner = false;
          },
          error => {
            console.log(error);
            this.showSpinner = false;
          }
        );

      },
      error => console.log(error),
    );
  }

  serializePlayRates(playRates) {
    this.globalPickRate = Object.values(playRates).reduce((partialSum: number, a: number) => partialSum + a, 0) as number;

    Object.entries(playRates).forEach(([key, value], number) => {
      this.serializedPlayRates[number].value = ((value as number) * 100 / this.globalPickRate);
    });
  }
}
