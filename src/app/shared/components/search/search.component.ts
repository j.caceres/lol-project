import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  summonerName: string = '';

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {

  }

  getSummonerData(): void {
    this.router.navigate(['summoner', this.summonerName]);
  }
}
