import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() fullScreen = false;
  @Input() show = false;
  @Input() text: string;

  constructor() { }

  ngOnInit(): void {
  }

}
