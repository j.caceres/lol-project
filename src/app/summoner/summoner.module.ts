import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummonerDashboardComponent } from './components/summoner-dashboard/summoner-dashboard.component';
import { RouterModule } from '@angular/router';
import { SummonerRoutingModule } from './summoner-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SummonerRoutingModule,
    SharedModule
  ],
  declarations: [
    SummonerDashboardComponent,
  ],
})
export class SummonerModule { }
