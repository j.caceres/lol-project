import { Champion } from '../../../core/classes/Champion';
import { SummonerService } from 'src/app/core/services/summoner.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Summoner } from 'src/app/core/classes/Summoner';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageDataService } from 'src/app/core/services/storage-data.service';
import { Utils } from 'src/app/core/classes/utils';

@Component({
  selector: 'app-summoner-dashboard',
  templateUrl: './summoner-dashboard.component.html',
  styleUrls: ['./summoner-dashboard.component.scss']
})
export class SummonerDashboardComponent implements OnInit {

  summoner: Summoner;
  summonerExists: boolean;

  allChampions: Champion[];
  allItems: any = [];
  allSummonerSpells: any = [];
  allRunes: any = [];

  componentSubscriptions: { [key: string]: Subscription } = {};

  dato = Utils.datos;
  showSpinner = false;
  panelOpenState = false;

  matchHistoryPage = 1;

  getImageItemFullUrl = Utils.getImageItemFullUrl;
  getImageSummonerSpellFullUrl = Utils.getImageSummonerSpellFullUrl;
  getImageRuneFullUrl = Utils.getImageRuneFullUrl;

  constructor(
    private activatedRoute: ActivatedRoute,
    private storageDataService: StorageDataService,
    private summonerService: SummonerService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.subscribeToSummonerParam();
    this.subscribeToChampions();
    this.subscribeToItems();
    this.subscribeToSummonerSpells();
    this.subscribeToRunes();
  }

  subscribeToSummonerParam(): void {
    this.componentSubscriptions.participantStageParam$ = this.activatedRoute.paramMap.subscribe(
      params => {
        if (params.get('summonerName')) {
          this.summonerExists = true;
          this.showSpinner = true;
          this.storageDataService.getAllChampions();
          this.storageDataService.getAllItems();
          this.storageDataService.getAllSummonerSpells();
          this.storageDataService.getAllRunes();
          this.summonerService.getSummonerByName(params.get('summonerName')).subscribe(
            summoner => {
              this.showSpinner = false;
              this.summoner = summoner;
              this.matchHistoryPage = 1;
              this.serializeMatchHistory();
            },
            error => {
              this.router.navigate(['search_error']);
              this.showSpinner = false;
              console.log(error);
            }
          );
        }
      },
      error => console.log(error),
    );
  }

  subscribeToChampions(): void {
    this.storageDataService.champions$.subscribe(
      champs => this.allChampions = champs
    );
  }

  subscribeToItems(): void {
    this.storageDataService.items$.subscribe(
      items => this.allItems = items
    );
  }

  subscribeToSummonerSpells(): void {
    this.storageDataService.summonerSpells$.subscribe(
      summonerSpells => this.allSummonerSpells = summonerSpells
    );
  }

  subscribeToRunes(): void {
    this.storageDataService.runes$.subscribe(
      runes => this.allRunes = runes
    );
  }

  getMoreMatchHistory(): void {
    this.componentSubscriptions.summoner$ = this.summonerService.getSummonerHistoryMatch(this.summoner.name, this.matchHistoryPage++).subscribe(
      matches => {
        this.summoner.matchHistory = this.summoner.matchHistory.concat(matches);
        this.serializeMatchHistory();
      }
    );
  }

  serializeMatchHistory(): void {
    this.summoner.matchHistory.forEach(
      match => {
        match.teams.forEach(
          team => {
            team.participants.forEach(
              participant => {
                participant.image = this.allChampions.find(champion => {
                  if (champion.championId === participant.championId) {
                    return champion.image;
                  }
                });

                const tempItems = [
                  participant.stats.item0,
                  participant.stats.item1,
                  participant.stats.item2,
                  participant.stats.item3,
                  participant.stats.item4,
                  participant.stats.item5,
                  participant.stats.item6];

                participant.items = [];
                participant.items = this.allItems.reduce((itemTotal, currentItem, index) => {
                  if (tempItems.includes(currentItem.id)) {
                    itemTotal[index] = currentItem;
                  }
                  return itemTotal;
                }, {});

                participant.summonerSpells = {
                  D: {},
                  F: {}
                };
                participant.summonerSpells.D = this.allSummonerSpells.find(spell => spell.id === participant.summonerSpellDId);
                participant.summonerSpells.F = this.allSummonerSpells.find(spell => spell.id === participant.summonerSpellFId);

                const tempPerks = Object.keys(participant.perks).map(Number);
                participant.runes = [];
                participant.runes = this.allRunes.reduce((runeTotal, currentRune, index) => {
                  if (tempPerks.includes(currentRune.id)) {
                    runeTotal[index] = currentRune;
                  }
                  return runeTotal;
                }, []);
                participant.runes = participant.runes.sort(rune => rune.tier);

                if (participant.summonerName === this.summoner.name) {
                  match.currentSummoner = participant;
                  match.summonerWin = team.isWinner;
                }
              }
            );

          }

        );
      }
    );

  }
}

