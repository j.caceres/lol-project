import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SummonerDashboardComponent } from './components/summoner-dashboard/summoner-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: SummonerDashboardComponent,
  },
  {
    path: ':summonerName',
    component: SummonerDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SummonerRoutingModule { }
