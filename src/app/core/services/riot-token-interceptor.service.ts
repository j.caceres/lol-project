import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class RiotTokenInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const riotToken = 'prueba';
    // const modifiedReq = req.clone({
    //   headers: req.headers.set('X-Riot-Token', riotToken),
    // });
    const modifiedReq = req;
    return next.handle(modifiedReq);
  }
}
