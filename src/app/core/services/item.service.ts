
import { Champion } from 'src/app/core/classes/Champion';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Url } from '../classes/Url';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private httpClient: HttpClient) { }

  getAllItems(): Observable<any> {
    return this.httpClient.get<any>(`${Url.ALL_ITEMS}`);
  }
}