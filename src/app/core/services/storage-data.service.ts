import { ChampionService } from './champion.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Champion } from 'src/app/core/classes/Champion';
import { SummonerService } from 'src/app/core/services/summoner.service';
import { ItemService } from './item.service';

@Injectable({
  providedIn: 'root'
})
export class StorageDataService {

  champions: Champion[] = [];
  champions$: BehaviorSubject<Champion[]> = new BehaviorSubject(this.champions);
  items: any = [];
  items$: BehaviorSubject<any> = new BehaviorSubject(this.items);
  summonerSpells: any = [];
  summonerSpells$: BehaviorSubject<any> = new BehaviorSubject(this.summonerSpells);
  runes: any = [];
  runes$: BehaviorSubject<any> = new BehaviorSubject(this.runes);

  constructor(
    private championService: ChampionService,
    private itemService: ItemService,
    private summonerService: SummonerService
  ) {
  }

  getAllChampions(): void {
    if (!this.champions.length) {
      this.championService.getAllChampions().subscribe(
        champions => {
          this.champions = champions;
          this.emitChampions();
        }
      );
    }
  }

  getAllItems(): void {
    if (!this.items.length) {
      this.itemService.getAllItems().subscribe(
        items => {
          this.items = items;
          this.emitItems();
        }
      );
    }
  }

  getAllSummonerSpells(): void {
    if (!this.summonerSpells.length) {
      this.summonerService.getAllSummonerSpells().subscribe(
        summonerSpells => {
          this.summonerSpells = summonerSpells;
          this.emitSummonerSpells();
        }
      );
    }
  }

  getAllRunes(): void {
    if (!this.summonerSpells.length) {
      this.summonerService.getAllRunes().subscribe(
        runes => {
          this.runes = runes;
          this.emitRunes();
        }
      );
    }
  }

  emitChampions(): void {
    this.champions$.next(this.champions);
  }

  emitItems(): void {
    this.items$.next(this.items);
  }

  emitSummonerSpells(): void {
    this.summonerSpells$.next(this.summonerSpells);
  }

  emitRunes(): void {
    this.runes$.next(this.runes);
  }

}
