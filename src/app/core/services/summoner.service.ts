import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Url } from '../classes/Url';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Summoner } from '../classes/Summoner';
import { MatchGame } from '../classes/MatchGame';


@Injectable({
  providedIn: 'root'
})
export class SummonerService {

  constructor(private httpClient: HttpClient) { }

  getSummonerByName(name: string): Observable<Summoner> {
    return this.httpClient.get<any>(`${Url.SUMMONER_SUMMONER_DATA.replace('{summoner}', name)}`);
  }

  getSummonerHistoryMatch(name: string, page: number): Observable<MatchGame[]> {
    return this.httpClient.put<MatchGame[]>(`${Url.SUMMONER_MATCH_HISTORY.replace('{summoner}', name)}`, { page: page });
  }

  getChampSummonerData(summoner: string, champion: string): Observable<any> {
    return this.httpClient.get<any>(`${Url.SUMMONER.replace('{name}', summoner).replace('{champion}', champion)}`);
  }

  getAllSummonerSpells(): Observable<any> {
    return this.httpClient.get<any>(`${Url.ALL_SUMMONER_SPELLS}`);
  }

  getAllRunes(): Observable<any> {
    return this.httpClient.get<any>(`${Url.ALL_RUNES}`);
  }
}
