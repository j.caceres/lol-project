import { Champion } from 'src/app/core/classes/Champion';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Url } from '../classes/Url';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChampionService {

  constructor(private httpClient: HttpClient) { }

  getChampionByName(name: string): Observable<Champion> {
    return this.httpClient.get<Champion>(`${Url.CHAMPION_CHAMPION_DATA.replace('{champion}', name)}`);
  }

  getAllChampions(): Observable<Champion[]> {
    return this.httpClient.get<Champion[]>(`${Url.ALL_CHAMPION}`);
  }
}
