import { Champion, SummonerChampion } from './Champion';
import { League } from "./League";
import { MatchGame } from './MatchGame';

export class Summoner {

  name: string;
  level: number;
  icon: string;
  leagues: {
    solo: League,
    flex: League
  };
  champions: SummonerChampion[];
  matchHistory: MatchGame[] = [];
}
