export class Champion {
  name: string;
  image: string;
  key: string;
  tags: string[];
  championId: number;
  play_rates: {
    "top": number,
    "jungle": number,
    "middle": number,
    "bottom": number,
    "support": number
  }
  skins: {
    'name': string,
    "image": string,
    "splash": string
  }[];
  ally_tips: string[];
  enemy_tips: string[];
  resume: string;
  passive: {
    'name': string,
    "image": string,
    "description": string,
  }
  spells: {
    "name": string,
    "image": string,
    "description": string,
    "keywords": string[]
  }
}

export class SummonerChampion {
  name: string;
  image: string;
  tags: string[];
}
