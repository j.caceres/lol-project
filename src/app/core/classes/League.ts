
export enum TIER_VALUES {
  CHALLENGER = 'CHALLENGER',
  GRANDMASTER = 'GRANDMASTER',
  MASTER = 'MASTER',
  DIAMOND = 'DIAMOND',
  PLATINUM = 'PLATINUM',
  GOLD = 'GOLD',
  SILVER = 'SILVER',
  BRONZE = 'BRONZE',
  IRON = 'IRON',
  UNRANKED = 'UNRANKED'
}

export class League {

  tier: TIER_VALUES;
  division: string;
  wins: number;
  losses: number;
  points: number;
  name: string;
}
