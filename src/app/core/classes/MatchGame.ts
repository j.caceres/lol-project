export class MatchGame {
  participants: [];
  gameDuration: number;
  matchId: number;
  teams: any = [];
  currentSummoner: any;
  summonerWin: false;
}
