import { environment } from '../../../environments/environment';

export class Url {
  static BASE_URL: string = environment.BASE_URL;

  static SUMMONER: string = Url.BASE_URL.concat('v1/summoner/');
  static SUMMONER_NAME: string = Url.SUMMONER.concat('{summoner}/')
  static SUMMONER_DATA_LITE: string = Url.SUMMONER_NAME.concat('lite/');
  static SUMMONER_SUMMONER_DATA: string = Url.SUMMONER_NAME.concat('full_summoner_data/');
  static SUMMONER_CHAMP_DATA: string = Url.SUMMONER_NAME.concat('champ/');
  static SUMMONER_MATCH_HISTORY: string = Url.SUMMONER_NAME.concat('match_history/');
  static ALL_SUMMONER_SPELLS: string = Url.SUMMONER.concat('all_summoner_spells/');
  static ALL_RUNES: string = Url.SUMMONER.concat('all_runes/');

  static CHAMPION: string = Url.BASE_URL.concat('v1/champion/');
  static ALL_CHAMPION: string = Url.CHAMPION.concat('all_champions/');
  static CHAMPION_CHAMPION: string = Url.CHAMPION.concat('{champion}');
  static CHAMPION_CHAMPION_DATA: string = Url.CHAMPION_CHAMPION.concat('/champion/');

  static ITEM: string = Url.BASE_URL.concat('v1/item/');
  static ALL_ITEMS: string = Url.ITEM.concat('all_items/');
}
