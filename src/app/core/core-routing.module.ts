
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorPageComponent } from '../shared/components/error-page/error-page.component';
import { Utils } from './classes/utils';
import { LayoutComponent } from './components/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'summoner',
        loadChildren: () => import('../summoner/summoner.module').then(m => m.SummonerModule)
      },
      {
        path: 'champion',
        loadChildren: () => import('../champion/champion.module').then(m => m.ChampionModule)
      },
      {
        path: 'analytics',
        component: ErrorPageComponent,
        data: {
          error: Utils.ERROR_PAGES_TYPES.WORK_IN_PROGRESS
        }
      },
      {
        path: 'search_error',
        component: ErrorPageComponent,
        data: {
          error: Utils.ERROR_PAGES_TYPES.SEARCH_ERROR
        }
      },
      {
        path: 'error',
        component: ErrorPageComponent,
        data: {
          error: Utils.ERROR_PAGES_TYPES.ERROR
        }
      },
      {
        path: '**', component: ErrorPageComponent,
        data: {
          error: Utils.ERROR_PAGES_TYPES.NOT_VALID_URL
        }
      }

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
