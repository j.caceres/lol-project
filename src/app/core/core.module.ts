import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SearchComponent } from '../shared/components/search/search.component';
import { SpinnerComponent } from '../shared/components/spinner/spinner.component';
import { LayoutComponent } from './components/layout/layout.component';
import { CoreRoutingModule } from './core-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    CoreRoutingModule,
    SharedModule
  ],
  exports: [
  ],
  declarations: [
    NavbarComponent,
    LayoutComponent,
    FooterComponent
  ],
  providers: [],

})
export class CoreModule {
}
