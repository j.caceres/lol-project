import { MainDashboardComponent } from './components/main-dashboard/main-dashboard.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    DashboardRoutingModule,
  ],
  declarations: [
    MainDashboardComponent,
  ],
  exports: [
  ],
  providers: [],
})
export class DashboardModule {
}
