import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Summoner } from 'src/app/core/classes/Summoner';
import { SummonerService } from 'src/app/core/services/summoner.service';
import { StorageDataService } from '../../../core/services/storage-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.scss']
})
export class MainDashboardComponent implements OnInit {

  componentSubscriptions: { [key: string]: Subscription } = {};

  constructor(
  ) { }

  ngOnInit(): void {
  }

}
